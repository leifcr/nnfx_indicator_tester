//+------------------------------------------------------------------+
//|                                            nnfxsignal_tester.mq5 |
//|                              Copyright 2019-2021. Leif Ringstad  |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019-2021, Leif Ringstad."
#property version "1.08"
//+------------------------------------------------------------------+
//| Include                                                          |
//+------------------------------------------------------------------+
// #include <Expert\Expert.mqh>
//--- available signals
// #include <Expert\Signal\SignalSAR.mqh>
#include "TradingSignal.mqh"
#include "defines.mqh"
#include "experts/TrendExpert.mqh"

// Baselines
// Confirmation indicators
#include "signals/ChangeAbleSignal.mqh"
#include "reporters/HistoryDealReporter.mqh"
// #include "signals/SuperTrendSignal.mqh"

//--- available trailing
#include <Expert/Trailing/TrailingNone.mqh>
//--- available money management
#include <Expert/Money/MoneyFixedLot.mqh>
//+------------------------------------------------------------------+
//| Inputs                                                           |
//+------------------------------------------------------------------+
//--- inputs for expert
input string Expert_Title = "nnfxsignal_tester";  // Document name
ulong Expert_MagicNumber  = 4242;                 //
bool Expert_EveryTick     = false;                //
input int Signal_ThresholdOpen  = 100;  // Signal threshold value to open [0...100]
input int Signal_ThresholdClose = 100;  // Signal threshold value to close [0...100]
//--- inputs for money
input double Money_FixLot_Percent = 99.0;   // Max percentage loss on single trade (Set to 99% on signal tester, as only signal should be tested)
input double Money_FixLot_Lots    = 0.01;   // Fixed volume for each trade
input bool inp_allow_multiple_trades = false; // Allow multiple trades in same directions
input bool inp_allow_hedge_trades = true; // Allow both short and long position at same time.
input bool inp_exit_on_signal = false; // Exit on signal or on TP/SL
// In case ATR is to tight?

// input double MinSL = 75.0; // The minimum range for SL if the given indicator has too low SL for lines

CArrayString *comment_array;

//+------------------------------------------------------------------+
//| Global expert object                                             |
//+------------------------------------------------------------------+
TrendExpert trend_expert;
// Global indicators
TradingSignal *main_signal;
HistoryDealReporter *reporter;
//+------------------------------------------------------------------+
//| Initialization function of the expert                            |
//+------------------------------------------------------------------+
int OnInit() {
  // Initialize comment array
  comment_array = new CArrayString();
  if (comment_array == NULL) {
    printf("Object create error: Comment Array! FATAL");
    return (INIT_FAILED);
  }

  //--- Initializing expert
  if (!trend_expert.Init(Symbol(), Period(), Expert_EveryTick, Expert_MagicNumber)) {
    //--- failed
    printf(__FUNCTION__ + ": error initializing expert");
    trend_expert.Deinit();
    return (INIT_FAILED);
  }
  //--- Creating signal
  main_signal = new TradingSignal;
  if (main_signal == NULL) {
    //--- failed
    printf(__FUNCTION__ + ": error creating Main signal");
    trend_expert.Deinit();
    return (INIT_FAILED);
  }

  //---
  trend_expert.InitSignal(main_signal);
  trend_expert.setMainSignal(main_signal);
  trend_expert.setAllowMultipleTrades(inp_allow_multiple_trades);
  trend_expert.setAllowHedge(inp_allow_hedge_trades);
  trend_expert.setExitOnSignal(inp_exit_on_signal);

  comment_array.Add("MainSignal: COMMENT_MAIN_SIGNAL");
  comment_array.Add("MainSignal: COMMENT_MAIN_SIGNAL_DEBUG");
  main_signal.SetCommentStringArray(comment_array);

  // Add confirmation indicators as filters
  comment_array.Add("Indicator COMMENT_TEST_SIGNAL: ");
  comment_array.Add("Indicator COMMENT_TEST_SIGNAL_DEBUG: ");
  ChangeAbleSignal *cs = new ChangeAbleSignal;
  main_signal.AddFilter(cs);
  cs.SetCommentStringArray(comment_array);

  // Nullify comments
  comment_array.Update(COMMENT_MAIN_SIGNAL, "");
  comment_array.Update(COMMENT_MAIN_SIGNAL_DEBUG, "");
  comment_array.Update(COMMENT_TEST_SIGNAL, "");
  comment_array.Update(COMMENT_TEST_SIGNAL_DEBUG, "");

  //--- Creation of trailing object
  CTrailingNone *trailing = new CTrailingNone;
  if (trailing == NULL) {
    //--- failed
    printf(__FUNCTION__ + ": error creating trailing");
    trend_expert.Deinit();
    return (INIT_FAILED);
  }
  //--- Add trailing to expert (will be deleted automatically))
  if (!trend_expert.InitTrailing(trailing)) {
    //--- failed
    printf(__FUNCTION__ + ": error initializing trailing");
    trend_expert.Deinit();
    return (INIT_FAILED);
  }
  //--- Set trailing parameters
  //--- Creation of money object
  CMoneyFixedLot *money = new CMoneyFixedLot;
  if (money == NULL) {
    //--- failed
    printf(__FUNCTION__ + ": error creating money");
    trend_expert.Deinit();
    return (INIT_FAILED);
  }
  //--- Add money to expert (will be deleted automatically))
  if (!trend_expert.InitMoney(money)) {
    //--- failed
    printf(__FUNCTION__ + ": error initializing money");
    trend_expert.Deinit();
    return (INIT_FAILED);
  }
  //--- Set money parameters
  money.Percent(Money_FixLot_Percent);
  money.Lots(Money_FixLot_Lots);
  //--- Check all trading objects parameters
  if (!trend_expert.ValidationSettings()) {
    //--- failed
    trend_expert.Deinit();
    return (INIT_FAILED);
  }
  //--- Tuning of all necessary indicators
  if (!trend_expert.InitIndicators()) {
    //--- failed
    printf(__FUNCTION__ + ": error initializing indicators");
    trend_expert.Deinit();
    return (INIT_FAILED);
  }

  reporter = new HistoryDealReporter(cs, main_signal, &trend_expert);
  //--- ok
  return (INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Deinitialization function of the expert                          |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) { trend_expert.Deinit(); }
//+------------------------------------------------------------------+
//| "Tick" event handler function                                    |
//+------------------------------------------------------------------+
void OnTick() {
  trend_expert.OnTick();
  Comment(CommentArrayToString());
}
//+------------------------------------------------------------------+
//| "Trade" event handler function                                   |
//+------------------------------------------------------------------+
void OnTrade() { trend_expert.OnTrade(); }
//+------------------------------------------------------------------+
//| "Timer" event handler function                                   |
//+------------------------------------------------------------------+
void OnTimer() { trend_expert.OnTimer(); }
//+------------------------------------------------------------------+

// //+------------------------------------------------------------------+
// //| Tester function                                                  |
// //+------------------------------------------------------------------+
double OnTester() {
  // if (start==0) start = TimeCurrent();
  reporter.BuildAndWriteReport();
  return 0.0;
}

string CommentArrayToString() {
  string ret = "";
  for(int i = 0; i < comment_array.Total(); i++)
  {
    ret += comment_array.At(i);
  }
  return ret;
}