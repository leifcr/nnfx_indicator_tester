//+------------------------------------------------------------------+
//|                                         HistoryDealReporter.mqh  |
//|                        Copyright 2019-2021, Leif Ringstad        |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, Leif."

#include <Files/FileTxt.mqh>
#include <Generic/Internal/HashFunction.mqh>
#include "../signals/ChangeAbleSignal.mqh"
#include "../TradingSignal.mqh"
#include "../experts/TrendExpert.mqh"


/*
Windows structure for a GetSystemTime() or GetLocalTime() call from http://msdn.microsoft.com/en-us/library/ms724950%28v=vs.85%29.aspx
typedef struct _SYSTEMTIME {
  WORD wYear;
  WORD wMonth;
  WORD wDayOfWeek;
  WORD wHour;
  WORD wMinute;
  WORD wSecond;
  WORD wDay;
  WORD wMilliseconds;
} SYSTEMTIME, *PSYSTEMTIME;
*/

// MQL5 equivalent struct:
struct SYSTEMTIME
{
  ushort wYear;         // 2014 etc
  ushort wMonth;        // 1 - 12
  ushort wDayOfWeek;    // 0 - 6 with 0 = Sunday
  ushort wDay;          // 1 - 31
  ushort wHour;         // 0 - 23
  ushort wMinute;       // 0 - 59
  ushort wSecond;       // 0 - 59
  ushort wMilliseconds; // 0 - 999
};

#import "kernel32.dll"
void GetSystemTime(SYSTEMTIME &SystemTimeStruct);
#import

input bool ReporterEnabled = true; // Writing to CSV Report Enabled or Disabled


class HistoryDealReporter
{
private:
  struct HISTORYCLOSEDDEAL {
    HISTORYCLOSEDDEAL() : commision(0.0), swap(0.0), profit(0.0), volume(0.0), win(0), sl(0), tp(0), type(DEAL_TAX), symbol("") {};
    double commision;
    double swap;
    double profit;
    double volume;
    string symbol;
    // string comment,

    int win; // 0 for loss, 1 for win (to make stats easier to build)
    int sl; // 0 if not hit, 1 if hit
    int tp; // 0 if not hit, 1 if hit
    ENUM_DEAL_TYPE type; // buy or sell
    // double price,
  };

  struct DEALTOTALS {
    DEALTOTALS() : symbol(""),
      period(0), shorts(0), longs(0),
      short_wins(0), long_wins(0), short_win_percentage(0), win_percentage(0),
      short_sl(0), long_sl(0), short_tp(0), long_tp(0), tp_percentage(0), sl_percentage(0),
      profit(0.0), net_profit_ex_commision(0.0), net_profit(0.0),
      volume(0.0), commision(0.0), swap(0.0) {};
    string symbol;
    int period; // String or int?
    int shorts;
    int longs;
    int short_wins;
    int long_wins;
    int short_sl;
    int long_sl;
    int short_tp;
    int long_tp;
    double short_win_percentage;
    double long_win_percentage;
    double win_percentage;
    double tp_percentage;
    double sl_percentage;
    double profit;
    double net_profit_ex_commision;
    double net_profit;
    double volume;
    double commision;
    double swap;
    double dd;
  };

  /* data */
  ChangeAbleSignal *m_signal;
  TradingSignal *m_trading_signal;
  TrendExpert *m_trend_expert;

  // SEt initial dealtype to tax, as that is not used.
  CFileTxt m_csvfile;
  int m_filehandle;
  // HISTORYCLOSEDDEAL deals[];
  // DEALTOTALS totals;

  bool m_reporter_enabled;

public:
  HistoryDealReporter(ChangeAbleSignal *signal, TradingSignal *trading_signal, TrendExpert *tx);
  ~HistoryDealReporter();

  bool BuildAndWriteReport();
private:
  void AppendCSVData();
  void EnsureHeadersInCSVFile();
  bool buildDeals(HISTORYCLOSEDDEAL &deals[]);
  bool parseDeals(HISTORYCLOSEDDEAL &deals[], DEALTOTALS &totals);
  bool WriteDealsTotal(DEALTOTALS &totals, int nest);

  int SettingsIdentifier();

  bool writeHeaders();

  string MonthDurations();
  double RealOrFakeCommision(double commision, double volume);
  // void setStartTime();
};

HistoryDealReporter::HistoryDealReporter(ChangeAbleSignal *signal, TradingSignal *trading_signal, TrendExpert *tx) : m_reporter_enabled(ReporterEnabled)
{
  m_signal = signal;
  m_trading_signal = trading_signal;
  m_trend_expert = tx;
}

HistoryDealReporter::~HistoryDealReporter()
{
// // Ensure file is closed when object is destroyed?
//   // m_csvfile.Close();
}

bool HistoryDealReporter::BuildAndWriteReport() {
  if (!m_reporter_enabled) {
    return false;
  }

  string filename = "NNFX_Indicator_Testing.csv";
  m_filehandle = m_csvfile.Open(filename, FILE_WRITE|FILE_CSV|FILE_READ|FILE_COMMON, ',');
  if (m_filehandle == INVALID_HANDLE) {
     Print("HistoryDealReporter: Failed to open the file by the absolute path ");
     Print("HistoryDealReporter: Error code ",GetLastError());
  }
  EnsureHeadersInCSVFile();

  HISTORYCLOSEDDEAL deals[];
  DEALTOTALS totals;
  if (!buildDeals(deals)) {
    Print("HistoryDealReporter: Cannot Build deals for report");
    Print("HistoryDealReporter: Error code: ", GetLastError());
    m_csvfile.Close();
    return false;
  }

  if (!parseDeals(deals, totals)) {
    Print("HistoryDealReporter: Cannot parse deals for report");
    Print("HistoryDealReporter: Error code: ", GetLastError());
    m_csvfile.Close();
    return false;
  }

  if(!WriteDealsTotal(totals, 0)) {
    Print("HistoryDealReporter: Cannot not write deals summary in CSV file");
    Print("HistoryDealReporter: Error code: ", GetLastError());
    m_csvfile.Close();
    return false;
  }
  m_csvfile.Close();
  Print("HistoryDealReporter: Written report to ", filename);
  return true;
}

void HistoryDealReporter::EnsureHeadersInCSVFile() {
  if (m_filehandle == INVALID_HANDLE) {
    return;
  }
  // Seek to beginning of file
  m_csvfile.Seek(0, SEEK_SET);
  // read first line
  string headers = m_csvfile.ReadString();
  // Check if first line includes "indicator" and "symbol" and "period"
  if ((StringFind(headers, "indicator") == -1) && (StringFind(headers, "symbol") == -1)) {
    // Create headers if missing
    m_csvfile.Seek(0, SEEK_SET);
    if (!writeHeaders()) {
      Print("HistoryDealReporter: Could not write CSV headers!");
      Print("HistoryDealReporter: Error code: ", GetLastError());
    }
  }
}


bool HistoryDealReporter::writeHeaders() {
  uint written = 0;
  written = FileWrite(m_csvfile.Handle(),
  "indicator",
  "symbol",
  "total_trades",
  "total_wins",
  "total_win_percentage",
  "period",
  "starttime",
  "endtime",
  "months",
  "shorts",
  "longs",
  "short_wins",
  "long_wins",
  "short_sl",
  "long_sl",
  "short_tp",
  "long_tp",
  "sl_percentage",
  "tp_percentage",
  "short_percentage",
  "long_percentage",
  "net_profit",
  "net_profit_ex_commision",
  "profit",
  "volume",
  "commision",
  "swap",
  "settingsidentifier",
  "ATR_SL",
  "ATR_TP",
  "indicator_check_type",
  "input_value_0",
  "input_value_1",
  "input_value_2",
  "input_value_3",
  "input_value_4",
  "input_value_5",
  "input_value_6",
  "input_value_7",
  "input_value_8",
  "input_value_9",
  "input_type_0",
  "input_type_1",
  "input_type_2",
  "input_type_3",
  "input_type_4",
  "input_type_5",
  "input_type_6",
  "input_type_7",
  "input_type_8",
  "input_type_9",
  "lower_cross_value",
  "upper_cross_value",
  "check_buffer_0",
  "check_buffer_1",
  "check_buffer_2",
  "check_buffer_3",
  "check_idx",
  "allow_multiple_trades",
  "allow_hedge_trades",
  "exit_on_signal",
  "testdate"
  // Not possible to get data_quality_percentage?
  // "data_quality_percentage",
  // "modeling"
  );

  if (written ==0) {
    return false;
  }
  return true;
}

bool HistoryDealReporter::WriteDealsTotal(DEALTOTALS &totals, int nest) {
  // Seek to end of file
  // Must retry writing, since another thread might use it...
  if (nest >= 5) {
    return false;
  }
  uint written = 0;
  SYSTEMTIME st;
  GetSystemTime(st);

  m_csvfile.Seek(0, SEEK_END);
  written = FileWrite(m_csvfile.Handle(),
    m_signal.m_indicator_name, // indicator
    totals.symbol, // symbol
    totals.shorts + totals.longs, // total_trades
    totals.short_wins + totals.long_wins, // total_wins
    DoubleToString(totals.win_percentage, 1), // total_win_percentage
    EnumToString(m_signal.GetPeriod()), // period
    TimeToString(m_trend_expert.ReporterStartTime()), // starttime
    TimeToString(m_trend_expert.ReporterEndTime()), // endtime
    MonthDurations(), // months
    totals.shorts, // shorts
    totals.longs, // longs
    totals.short_wins, // short_wins
    totals.long_wins, // long_wins
    totals.short_sl, // short_sl
    totals.long_sl, // long_sl
    totals.short_tp, // short_tp
    totals.long_tp, // long_tp
    DoubleToString(totals.sl_percentage, 1), // sl_percentage
    DoubleToString(totals.tp_percentage, 1), // tp_percentage
    DoubleToString(totals.short_win_percentage, 1), // short_percentage
    DoubleToString(totals.long_win_percentage, 1), // long_percentage
    DoubleToString(totals.net_profit, 2), // net_profit
    DoubleToString(totals.net_profit_ex_commision, 2), // net_profit_ex_commision
    DoubleToString(totals.profit, 2), // profit
    DoubleToString(totals.volume, 2), // volume
    DoubleToString(totals.commision, 2), // commision
    DoubleToString(totals.swap, 2), // swap
    SettingsIdentifier(),
    m_trading_signal.ATR_SL(), // ATR_SL
    m_trading_signal.ATR_TP(), // ATR_TP
    EnumToString(m_signal.m_indicator_check_type), // indicator_check_type
    m_signal.m_input_value_0, // input_value_0
    m_signal.m_input_value_1, // input_value_1
    m_signal.m_input_value_2, // input_value_2
    m_signal.m_input_value_3, // input_value_3
    m_signal.m_input_value_4, // input_value_4
    m_signal.m_input_value_5, // input_value_5
    m_signal.m_input_value_6, // input_value_6
    m_signal.m_input_value_7, // input_value_7
    m_signal.m_input_value_8, // input_value_8
    m_signal.m_input_value_9, // input_value_9
    m_signal.m_input_type_0, // input_type_0
    m_signal.m_input_type_1, // input_type_1
    m_signal.m_input_type_2, // input_type_2
    m_signal.m_input_type_3, // input_type_3
    m_signal.m_input_type_4, // input_type_4
    m_signal.m_input_type_5, // input_type_5
    m_signal.m_input_type_6, // input_type_6
    m_signal.m_input_type_7, // input_type_7
    m_signal.m_input_type_8, // input_type_8
    m_signal.m_input_type_9, // input_type_9
    m_signal.m_upper_check_val, // lower_cross_value
    m_signal.m_lower_check_val, // upper_cross_value
    m_signal.m_check_buffer_0, // check_buffer_0
    m_signal.m_check_buffer_1, // check_buffer_1
    m_signal.m_check_buffer_2, // check_buffer_2
    m_signal.m_check_buffer_3, // check_buffer_3
    m_signal.m_check_idx, // check_idx
    m_trend_expert.allowMultipleTrades(), // allow_multiple_trades
    m_trend_expert.allowHedge(), // allow_hedge_trades
    m_trend_expert.exitOnSignal(), // exit_on_signal
    StringFormat("%d.%d.%d %d:%d:%d", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond)
    // ??? // data_quality_percentage
    // ??? // modeling
  );

  if (written == 0) {
    return WriteDealsTotal(totals, nest + 1);
  }
  return true;
}

int HistoryDealReporter::SettingsIdentifier() {
  string ident ="";
  ident += DoubleToString(m_trading_signal.ATR_SL(), 1);     // ATR_SL
  ident += DoubleToString(m_trading_signal.ATR_TP(), 1);     // ATR_TP
  ident += EnumToString(m_signal.m_indicator_check_type); // indicator_check_type
  ident += input_value_to_identifier_string(m_signal.m_input_value_0, m_signal.m_input_type_0); // input_value_0
  ident += input_value_to_identifier_string(m_signal.m_input_value_1, m_signal.m_input_type_1); // input_value_1
  ident += input_value_to_identifier_string(m_signal.m_input_value_2, m_signal.m_input_type_2); // input_value_2
  ident += input_value_to_identifier_string(m_signal.m_input_value_3, m_signal.m_input_type_3); // input_value_3
  ident += input_value_to_identifier_string(m_signal.m_input_value_4, m_signal.m_input_type_4); // input_value_4
  ident += input_value_to_identifier_string(m_signal.m_input_value_5, m_signal.m_input_type_5); // input_value_5
  ident += input_value_to_identifier_string(m_signal.m_input_value_6, m_signal.m_input_type_6); // input_value_6
  ident += input_value_to_identifier_string(m_signal.m_input_value_7, m_signal.m_input_type_7); // input_value_7
  ident += input_value_to_identifier_string(m_signal.m_input_value_8, m_signal.m_input_type_8); // input_value_8
  ident += input_value_to_identifier_string(m_signal.m_input_value_9, m_signal.m_input_type_9); // input_value_9
  ident += IntegerToString(m_signal.m_input_type_0); // input_type_0
  ident += IntegerToString(m_signal.m_input_type_1); // input_type_1
  ident += IntegerToString(m_signal.m_input_type_2); // input_type_2
  ident += IntegerToString(m_signal.m_input_type_3); // input_type_3
  ident += IntegerToString(m_signal.m_input_type_4); // input_type_4
  ident += IntegerToString(m_signal.m_input_type_5); // input_type_5
  ident += IntegerToString(m_signal.m_input_type_6); // input_type_6
  ident += IntegerToString(m_signal.m_input_type_7); // input_type_7
  ident += IntegerToString(m_signal.m_input_type_8); // input_type_8
  ident += IntegerToString(m_signal.m_input_type_9); // input_type_9
  ident += DoubleToString(m_signal.m_upper_check_val, 2); // lower_cross_value
  ident += DoubleToString(m_signal.m_lower_check_val, 2); // upper_cross_value
  ident += IntegerToString(m_signal.m_check_buffer_0); // check_buffer_0
  ident += IntegerToString(m_signal.m_check_buffer_1); // check_buffer_1
  ident += IntegerToString(m_signal.m_check_buffer_2); // check_buffer_2
  ident += IntegerToString(m_signal.m_check_buffer_3); // check_buffer_3
  ident += IntegerToString(m_signal.m_check_idx);      // check_idx
  ident += IntegerToString(m_trend_expert.allowMultipleTrades());
  ident += IntegerToString(m_trend_expert.allowHedge());
  ident += IntegerToString(m_trend_expert.exitOnSignal());
  // ident += IntegerToString(m_trend_expert.exitOnSignal()); // data_quality_percentage
  // ident += IntegerToString(m_trend_expert.exitOnSignal()); // modeling

  // Hash ident to get shorter/better string?
  return ::GetHashCode(ident);
  // return ident;
}

string input_value_to_identifier_string(double value, ENUM_DATATYPE type) {
  switch (type)
  {
    case TYPE_UINT:
    case TYPE_ULONG:
    case TYPE_UCHAR:
    case TYPE_BOOL:
    case TYPE_CHAR:
    case TYPE_SHORT:
    case TYPE_USHORT:
    case TYPE_INT:
    case TYPE_LONG:
    case TYPE_COLOR:
    case TYPE_DATETIME:
      return IntegerToString(value);
      break;
    case TYPE_FLOAT:
    case TYPE_DOUBLE:
      return DoubleToString(value, 3); // Only keep 3 decimals for identifier, and remove 0's ?
      break;
  }
  return "";
}

// Return 'fake 7$ commision unless real commision is reported', as $7 is used by pepperstone
double HistoryDealReporter::RealOrFakeCommision(double commision, double volume) {
  if (commision == 0.0) {
    return -(volume * 7);
  }

  if (commision > 0) {
     return -commision;
  }
  return commision;
}

string HistoryDealReporter::MonthDurations() {
  double months = (m_trend_expert.ReporterEndTime() - m_trend_expert.ReporterStartTime())/(60*60*24*12.0);
  return DoubleToString(months, 1);
}

bool HistoryDealReporter::buildDeals(HISTORYCLOSEDDEAL &deals[]) {
  // No results?
  datetime d = TimeCurrent();
  Print("HistoryDealReporter: Getting history until: " + TimeToString(d));
  if (!HistorySelect(0, d)) {
    Print("HistoryDealReporter: Cannot select history");
    return (false);
  }

  uint total_deals = HistoryDealsTotal();
  Print("HistoryDealReporter: Total Number of deals: " + IntegerToString(total_deals));
  uint counter = 0;
  ulong ticket_history_deal = 0;
  if (total_deals == 0) {
    return false;
  }
  //--- set the initial size of the array with a margin - by the number of deals in history
  ArrayResize(deals, total_deals);
  for (uint i = 0; i < total_deals; i++) {
    // select a deal
    if ((ticket_history_deal = HistoryDealGetTicket(i)) > 0) {
      ENUM_DEAL_ENTRY deal_entry = (ENUM_DEAL_ENTRY)HistoryDealGetInteger(ticket_history_deal, DEAL_ENTRY);
      ENUM_DEAL_TYPE type = (ENUM_DEAL_TYPE)HistoryDealGetInteger(ticket_history_deal, DEAL_TYPE);
      // only interested in trading operations, not buy/sell orders
      if ((type != DEAL_TYPE_BUY) && (type != DEAL_TYPE_SELL)) continue;
      // only deals that fix profits/losses
      if (deal_entry != DEAL_ENTRY_IN) {
        deals[counter].commision = HistoryDealGetDouble(ticket_history_deal, DEAL_COMMISSION);
        deals[counter].swap = HistoryDealGetDouble(ticket_history_deal, DEAL_SWAP);
        deals[counter].profit = HistoryDealGetDouble(ticket_history_deal, DEAL_PROFIT);
        // deals[counter].price = HistoryDealGetDouble(ticket_history_deal, DEAL_PRICE);
        deals[counter].volume = HistoryDealGetDouble(ticket_history_deal, DEAL_VOLUME);
        deals[counter].type = type;
        deals[counter].sl = ((HistoryDealGetInteger(ticket_history_deal, DEAL_REASON) == DEAL_REASON_SL) ? 1 : 0);
        deals[counter].tp = ((HistoryDealGetInteger(ticket_history_deal, DEAL_REASON) == DEAL_REASON_TP) ? 1 : 0);
        deals[counter].win = ((deals[counter].profit > 0) ? 1 : 0);
        deals[counter].symbol = HistoryDealGetString(ticket_history_deal, DEAL_SYMBOL);
        // deals[counter].comment
        counter++;
      }
    }
  }
  // Resize to counter, to remote unused data
  ArrayResize(deals, counter);
  return true;
}
bool HistoryDealReporter::parseDeals(HISTORYCLOSEDDEAL &deals[], DEALTOTALS &totals) {
  uint total_deals = ArraySize(deals);
  if (total_deals == 0) {
    Print("HistoryDealReporter: WARNING: No deals found!");
    return false;
  }
  totals.symbol = deals[0].symbol; // Should be same symbol for all entries/deals
  for (uint i = 0; i < total_deals; i++ ) {
    // totals.profit += deals[i].profit;
    totals.volume += deals[i].volume;
    totals.commision += deals[i].commision;
    totals.swap += deals[i].swap;
    if (deals[i].type == DEAL_TYPE_BUY) {
      totals.long_sl += deals[i].sl; // since it is 0 or 1, it can be added instead of checked
      totals.long_tp += deals[i].tp; // since it is 0 or 1, it can be added instead of checked
    }
    if (deals[i].type == DEAL_TYPE_SELL) {
      totals.short_sl += deals[i].sl; // since it is 0 or 1, it can be added instead of checked
      totals.short_tp += deals[i].tp; // since it is 0 or 1, it can be added instead of checked
    }
    // if (deals[i].type == DEAL_TYPE_BUY) {
    //   totals.longs++;
    //   if (deals[i].win) {
    //     totals.long_wins++;
    //   }
    // }
    // if (deals[i].type == DEAL_TYPE_SELL) {
    //   totals.shorts++;
    //   if (deals[i].win) {
    //     totals.short_wins++;
    //   }
    // }
  }
  totals.short_wins = TesterStatistics(STAT_PROFIT_SHORTTRADES);
  totals.long_wins  = TesterStatistics(STAT_PROFIT_LONGTRADES);
  totals.shorts = TesterStatistics(STAT_SHORT_TRADES);
  totals.longs = TesterStatistics(STAT_LONG_TRADES);
  totals.dd = TesterStatistics(STAT_BALANCEDD_PERCENT);
  totals.profit = TesterStatistics(STAT_PROFIT);

  totals.commision = RealOrFakeCommision(totals.commision, totals.volume);
  totals.net_profit_ex_commision = totals.profit + totals.swap;
  totals.net_profit = totals.profit + totals.swap + totals.commision;
  // Calc winning percentage, but also make sure we don't divide by 0...
  // Winrate is 0 for no trades... This usually only happens on some pairs on selected years when testing daily candles.
  if (totals.shorts == 0) {
    totals.short_win_percentage = 0;
  } else  {
    totals.short_win_percentage = ((totals.short_wins * 100.0)/totals.shorts);
  }

  if (totals.longs == 0) {
    totals.long_win_percentage = 0;
  } else  {
    totals.long_win_percentage = ((totals.long_wins * 100.0)/totals.longs);
  }
  if ((totals.longs == 0) && (totals.shorts == 0)) {
    totals.win_percentage = 0;
  } else {
    totals.win_percentage = ((totals.long_wins + totals.short_wins) * 100) / (totals.shorts + totals.longs);
  }

  if ((totals.long_sl == 0) && (totals.short_sl == 0)) {
  } else {
    totals.sl_percentage = ((totals.long_sl + totals.short_sl) * 100) / (totals.shorts + totals.longs);
  }

  if ((totals.long_tp == 0) && (totals.short_tp == 0)) {
  } else {
    totals.tp_percentage = ((totals.long_tp + totals.short_tp) * 100) / (totals.shorts + totals.longs);
  }


  // Print(StringFormat("calcstats  Shorts: %s, longs: %s, shorts_win: %s, longs_win: %s, profit: %s", 
  //       DoubleToString(totals.shorts, 0),
  //       DoubleToString(totals.longs, 0),
  //       DoubleToString(totals.short_wins, 0),
  //       DoubleToString(totals.long_wins, 0),
  //       DoubleToString(totals.profit, 2)
  //       ));

  // Print(StringFormat("testerstats Shorts: %s, longs: %s, shorts_win: %s, longs_win: %s, profit: %s, dd: %s, deposit: %s", 
  //       DoubleToString(shorts, 0),
  //       DoubleToString(longs, 0),
  //       DoubleToString(short_wins, 0),
  //       DoubleToString(long_wins, 0),
  //       DoubleToString(profit, 2),
  //       DoubleToString(dd, 2),
  //       DoubleToString(deposit, 0)
  //       ));
  return true;
}
