//+------------------------------------------------------------------+
//|                                         ExtendedPositionInfo.mqh |
//|                        Copyright 2019-2021, Leif Ringstad        |
//+------------------------------------------------------------------+
#include <Trade/PositionInfo.mqh>

class ExtendedPositionInfo : public CPositionInfo {
  public:
    bool SelectByDirectionAndMagic(const string symbol,const ulong magic, ENUM_POSITION_TYPE position_type);
};

// ENUM_POSITION_TYPE position_type, either POSITION_TYPE_BUY or POSITION_TYPE_SELL
bool ExtendedPositionInfo::SelectByDirectionAndMagic(const string symbol,const ulong magic, ENUM_POSITION_TYPE position_type) {
   bool res = false;
   uint total = PositionsTotal();

   for(uint i=0; i<total; i++) {
    string position_symbol=PositionGetSymbol(i);
    if(position_symbol==symbol && magic==PositionGetInteger(POSITION_MAGIC) && position_type == PositionGetInteger(POSITION_TYPE)) {
      res=true;
      break;
    }
  }

  return res;
}
