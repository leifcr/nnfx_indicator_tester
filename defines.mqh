//+------------------------------------------------------------------+
//|                                         defines.mqh              |
//|                        Copyright 2019-2021, Leif Ringstad        |
//+------------------------------------------------------------------+
/*
* Comment string Array
*/
#ifndef __COMMENT_FILTER__
  #define __COMMENT_FILTER__
  #define COMMENT_MAIN_SIGNAL 0
  #define COMMENT_MAIN_SIGNAL_DEBUG 1
  #define COMMENT_TEST_SIGNAL 2
  #define COMMENT_TEST_SIGNAL_DEBUG 3
  #define COMMENT_ARRAY_SIZE COMMENT_TEST_SIGNAL_DEBUG + 1
#endif

#ifndef XXDPO_APPLIED_PRICE
#define XXDPO_APPLIED_PRICE
enum Applied_price_      // Type of constant
  {
   PRICE_CLOSE_ = 1,     // Close
   PRICE_OPEN_,          // Open
   PRICE_HIGH_,          // High
   PRICE_LOW_,           // Low
   PRICE_MEDIAN_,        // Median Price (HL/2)
   PRICE_TYPICAL_,       // Typical Price (HLC/3)
   PRICE_WEIGHTED_,      // Weighted Close (HLCC/4)
   PRICE_SIMPLE,         // Simple Price (OC/2)
   PRICE_QUARTER_,       // Quarted Price (HLOC/4)
   PRICE_TRENDFOLLOW0_,  // TrendFollow_1 Price
   PRICE_TRENDFOLLOW1_   // TrendFollow_2 Price
  };
#endif

#ifndef XXDPO_SMOOTH_METHOD
#define XXDPO_SMOOTH_METHOD
enum Smooth_Method
  {
  MODE_SMA_,  //SMA
  MODE_EMA_,  //EMA
  MODE_SMMA_, //SMMA
  MODE_LWMA_, //LWMA
  MODE_JJMA,  //JJMA
  MODE_JurX,  //JurX
  MODE_ParMA, //ParMA
  MODE_T3,     //T3
  MODE_VIDYA,  //VIDYA
  MODE_AMA     //AMA
  };
#endif

