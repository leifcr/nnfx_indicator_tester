//+------------------------------------------------------------------+
//|                                         SignalBase.mqh           |
//|                        Copyright 2019-2021, Leif Ringstad        |
//+------------------------------------------------------------------+

#include <Arrays/ArrayString.mqh>
#include <Expert/ExpertSignal.mqh>

class SignalBase : public CExpertSignal {
 protected:
  CArrayString *m_comment_array;

 public:
  SignalBase(void);
  ~SignalBase(void){};
  void SetCommentStringArray(CArrayString *ptr) { m_comment_array = ptr; };
  // void SetCommentStringArrayIndex(int value) { m_comment_array_idx = value;
  // };
  string comment(int index);
  // void setComment(string);
  void setComment(string comment, int index);
  void setDirectionalComment(string signal_provider, double direction, int index);

  // virtual bool InitIndicators(CIndicators *indicators);

  // virtual bool CheckOpenLong(double &price, double &sl, double &tp, datetime &expiration);
  // virtual bool CheckOpenShort(double &price, double &sl, double &tp, datetime &expiration);

  void resetPrices(void);

  datetime zeroTime();

  int indexOffset();

 private:

  enum SignalStatus {
    SIGNAL_STATUS_IDLE  = 0,  // signal is idle...
    SIGNAL_STATUS_LONG  = 1,  // LONG signal
    SIGNAL_STATUS_SHORT = 2,  // SHORT signal
  };

  void InitializeZeroTime();

 protected:
  datetime last_bar_time_long;
  datetime last_bar_time_short;

  datetime m_zerotime;

  SignalStatus signalStatus;

  double m_sl_price;
  double m_tp_price;

  bool newBarLong();
  bool newBarShort();
};

SignalBase::SignalBase() {
  InitializeZeroTime();
  last_bar_time_long               = m_zerotime;
  last_bar_time_short              = m_zerotime;
  // last_bar_time_delete_check_long  = m_zerotime;
  // last_bar_time_delete_check_short = m_zerotime;
}

void SignalBase::InitializeZeroTime() {
  MqlDateTime inittime;
  inittime.year   = 1984;
  inittime.mon    = 1;
  inittime.day    = 1;
  inittime.hour   = 0;
  inittime.min    = 0;
  inittime.sec    = 0;
  m_zerotime = StructToTime(inittime);
}

// Offset if time of current bar is not same as startindex
// Seems like a bug in MT5.
// Solution:
// Check if tick time m_symbol.Time() == Time[StartIndex()) or Time(StartIndex()-1)

int SignalBase::indexOffset() {
  // Can use iBarShift?
  // Or iTime
  // Or CopyTime
  int start_idx = StartIndex();
  datetime t = iTime(NULL, PERIOD_CURRENT, 0);
  datetime y = m_symbol.Time();
  int idx = iBarShift(NULL, PERIOD_CURRENT, m_symbol.Time(), true);

  // if (m_symbol.Time() == Time[start_idx]) {
  //   return 0;
  // } else if (m_symbol.Time() == Time[start_idx-1]) {
  //   return -1;
  // }
  // // Log that it's not startindex or -1!
  // Print("SignalBase::indexOffset() : Symbol time is not Time[StartIndex] or Time[StartIndex -1]");
  return 0;
}

bool SignalBase::newBarLong() {
  if (last_bar_time_long == m_zerotime) {
    last_bar_time_long = m_symbol.Time();
    return false;
  }

  datetime new_bar_time = m_symbol.Time();

  if (last_bar_time_long == new_bar_time) {
    return false;
  }

  last_bar_time_long = new_bar_time;
  return true;
}

bool SignalBase::newBarShort() {
  if (last_bar_time_short == m_zerotime) {
    last_bar_time_short = m_symbol.Time();
    return false;
  }
  datetime new_bar_time = m_symbol.Time();

  if (last_bar_time_short == new_bar_time) {
    return false;
  }

  last_bar_time_short = new_bar_time;
  return true;
}

datetime SignalBase::zeroTime() {
  return m_zerotime;
}

string SignalBase::comment(int index) {
  string ret = "";
  if (m_comment_array == NULL) {
    return ret;
  }
  // return ""
  ret = m_comment_array.At(index);
  return ret;
}

// Set comment with index
void SignalBase::setComment(string comment, int index) {
  if (m_comment_array == NULL) {
    return;
  }
  m_comment_array.Update(index, comment);
}

void SignalBase::setDirectionalComment(string signal_provider, double direction, int index) {
  if (direction > 0) {
    setComment(signal_provider + ": Long", index);
  }
  else if (direction < 0) {
    setComment(signal_provider + ": Short", index);
  }
  else {
    setComment(signal_provider + ": -", index);
  }
}

void SignalBase::resetPrices(void) {
  m_base_price = 0.0;
  m_tp_price  = 0.0;
  m_sl_price   = 0.0;
}

