//+------------------------------------------------------------------+
//|                                         ChangeAbleSignal.mqh     |
//|                        Copyright 2019-2021, Leif Ringstad        |
//+------------------------------------------------------------------+

#include "../defines.mqh"
#include "../SignalBase.mqh"

// Implement this if TYPE_CHAR cannot be used for 'unused'
// enum DATATYPE_WITH_UNUSED {

// }

enum INDICATOR_CHECKTYPE {
  INDICATOR_CHECK_CROSSOVER = 0,             // Crossover for buffer 0 and 1
  INDICATOR_CHECK_CROSSOVER_0_1__2_3,        // Crossover long buff 0,1, short buff 2,3
  INDICATOR_CHECK_UPPER_LOWER_0,             // Ind val above upper - long, below lower - short, checks buffer 0
  INDICATOR_CHECK_BETWEEN_CORRECT_TREND,     // Ind val is between high and low, and trending in correct direction, checks buffer 0
  INDICATOR_CHECK_PRICE_CLOSE,               // BELOW price close - long, ABOVE price close - short (MA types)
  INDICATOR_CHECK_OPPOSITE_PRICE_CLOSE,      // Ind val is ABOVE close - long, and BELOW close - short (Supertrend/psar types)
  INDICATOR_CHECK_PRICE_CLOSE_SWITCHED,      // SWITCHED - BELOW price close - long, ABOVE price close
  INDICATOR_CHECK_ZERO_LINE_CROSS,           // Above 0 for long, below 0 for short
  INDICATOR_CHECK_CROSS_LEVEL,               // Above up level = long, below low level = short
  INDICATOR_CHECK_PRICE_CLOSE_0_1,           // BELOW (buf 0) close - long, ABOVE (buf 1) close - short
  INDICATOR_CHECK_SLOPE_BARS,                // Slope direction for indicator in X bars (upper val)
  INDICATOR_CHECK_SLOPE_CHANGED_BARS,        // Slope direction indicator in X bars AND change of slope (upper val)
  INDICATOR_CHECK_CROSS_LEVEL_0_1,           // Above up level = long (buf 0), below low level = short (buf 1)
  INDICATOR_CHECK_PRICE_CLOSE_0_1_SWITCHED,  // SWITCHED BELOW (buf 0) close - long, ABOVE (buf 1) close - short
  INDICATOR_CHECK_CROSS_LEVEL_0_1_EMPTY_VALUE, // cross level buffer 0 1 + not empty value, Above up level = long (buf 0), below low level = short (buf 1)
  // This cannot be used, as it will only show long/short on both signals. Can only be used to confirm trends
  // INDICATOR_CHECK_BETWEEN, // Check that value is between high and low
  // Not possible due to only showing trend?
  // INDICATOR_CHECK_ABOVE_LOWER_0, // Check that value is above value given in 'low', checks given buffer 0
  // INDICATOR_CHECK_BELOW_UPPER_1, // Check that value is below given in 'high' checks given buffer 1
};

/* Inputs */

// given when trading and/or backtesting input ENUM_TIMEFRAMES     period,            // period
//  input string              symbol,            // symbol name
input string indicator_name                    = "Folder/Indicator";
input INDICATOR_CHECKTYPE indicator_check_type = INDICATOR_CHECK_CROSSOVER;
input double input_value_0                     = 0.0;        // Changeind, Input value 0
input double input_value_1                     = 0.0;        // Changeind, Input value 1
input double input_value_2                     = 0.0;        // Changeind, Input value 2
input double input_value_3                     = 0.0;        // Changeind, Input value 3
input double input_value_4                     = 0.0;        // Changeind, Input value 4
input double input_value_5                     = 0.0;        // Changeind, Input value 5
input double input_value_6                     = 0.0;        // Changeind, Input value 6
input double input_value_7                     = 0.0;        // Changeind, Input value 7
input double input_value_8                     = 0.0;        // Changeind, Input value 8
input double input_value_9                     = 0.0;        // Changeind, Input value 9
input ENUM_DATATYPE input_type_0               = TYPE_CHAR;  // Changeind, Input type 0, TYPE_CHAR = UNUSED!
input ENUM_DATATYPE input_type_1               = TYPE_CHAR;  // Changeind, Input type 1, TYPE_CHAR = UNUSED!
input ENUM_DATATYPE input_type_2               = TYPE_CHAR;  // Changeind, Input type 2, TYPE_CHAR = UNUSED!
input ENUM_DATATYPE input_type_3               = TYPE_CHAR;  // Changeind, Input type 3, TYPE_CHAR = UNUSED!
input ENUM_DATATYPE input_type_4               = TYPE_CHAR;  // Changeind, Input type 4, TYPE_CHAR = UNUSED!
input ENUM_DATATYPE input_type_5               = TYPE_CHAR;  // Changeind, Input type 5, TYPE_CHAR = UNUSED!
input ENUM_DATATYPE input_type_6               = TYPE_CHAR;  // Changeind, Input type 6, TYPE_CHAR = UNUSED!
input ENUM_DATATYPE input_type_7               = TYPE_CHAR;  // Changeind, Input type 7, TYPE_CHAR = UNUSED!
input ENUM_DATATYPE input_type_8               = TYPE_CHAR;  // Changeind, Input type 8, TYPE_CHAR = UNUSED!
input ENUM_DATATYPE input_type_9               = TYPE_CHAR;  // Changeind, Input type 9, TYPE_CHAR = UNUSED!
input double upper_check_val                   = 0.0;        // Changeind, upper check value
input double lower_check_val                   = 0.0;        // Changeind, lower check value
input int check_buffer_0                       = -1;         // Changeind, check bufffer 0, set to -1 for unused
input int check_buffer_1                       = -1;         // Changeind, check bufffer 1, set to -1 for unused
input int check_buffer_2                       = -1;         // Changeind, check bufffer 2, set to -1 for unused
input int check_buffer_3                       = -1;         // Changeind, check bufffer 3, set to -1 for unused
input uint check_idx                           = 1;          // Index to check. Default 1. Last fully closed candle

class ChangeAbleSignal : public SignalBase {
 protected:
  CiCustom m_custom_ind;

 private:
  /* data */

  // Initially set to 11 (indicator name + 10 values)
  // Must be dynamic
  MqlParam mql_param[];
  // MqlParam mql_param[11];

  // Allow these to be public so reporter can get them!
 public:
  string m_indicator_name;
  INDICATOR_CHECKTYPE m_indicator_check_type;
  double m_input_value_0;
  double m_input_value_1;
  double m_input_value_2;
  double m_input_value_3;
  double m_input_value_4;
  double m_input_value_5;
  double m_input_value_6;
  double m_input_value_7;
  double m_input_value_8;
  double m_input_value_9;
  ENUM_DATATYPE m_input_type_0;
  ENUM_DATATYPE m_input_type_1;
  ENUM_DATATYPE m_input_type_2;
  ENUM_DATATYPE m_input_type_3;
  ENUM_DATATYPE m_input_type_4;
  ENUM_DATATYPE m_input_type_5;
  ENUM_DATATYPE m_input_type_6;
  ENUM_DATATYPE m_input_type_7;
  ENUM_DATATYPE m_input_type_8;
  ENUM_DATATYPE m_input_type_9;
  double m_upper_check_val;
  double m_lower_check_val;
  int m_check_buffer_0;
  int m_check_buffer_1;
  int m_check_buffer_2;
  int m_check_buffer_3;
  uint m_check_idx;

  int m_result;

 public:
  ChangeAbleSignal(/* args */);
  ~ChangeAbleSignal();

  //--- method of verification of settings
  virtual bool ValidationSettings(void);
  //--- method of creating the indicator and timeseries
  virtual bool InitIndicators(CIndicators *indicators);
  //--- methods of checking if the market models are formed
  virtual int LongCondition(void);
  virtual int ShortCondition(void);

  int LongConditionCrossover(int buffer_1, int buffer_2);
  int ShortConditionCrossover(int buffer_1, int buffer_2);

  int LongConditionAbove(int buffer, double threshold_value);
  int ShortConditionBelow(int buffer, double threshold_value);

  int LongConditionBetweenCorrectTrend(int buffer);
  int ShortConditionBetweenCorrectTrend(int buffer);

  int LongConditionPriceAboveIndicator(int buffer, int offset = 0);
  int LongConditionPriceBelowIndicator(int buffer, int offset = 0);

  int ShortConditionPriceAboveIndicator(int buffer, int offset = 0);
  int ShortConditionPriceBelowIndicator(int buffer, int offset = 0);

  int LongConditionCrossLevel(int buffer, double level);
  int ShortConditionCrossLevel(int buffer, double level);

  int LongConditionCrossLevelOrEmptyValue(int buffer, double level);
  int ShortConditionCrossLevelOrEmptyValue(int buffer, double level);

  int LongConditionSlope(int buffer, int bars);
  int ShortConditionSlope(int buffer, int bars);

  int LongConditionSlopeChanged(int buffer, int bars);
  int ShortConditionSlopeChanged(int buffer, int bars);

  // int LongConditionBetween(void);
  // int ShortConditionBetween(void);

  virtual double Direction(void);

  void BuildParams(void);
  void setMqlParam(MqlParam &p, double value, ENUM_DATATYPE type);

  ENUM_TIMEFRAMES GetPeriod() { return m_period; };

 private:
  // int checkIndex() { return (m_check_idx; + indexOffset() );};
  uint checkIndex() { return m_check_idx; };

  // void result(int value) { m_result = value };

  //--- methods for generating signals of entering the market
  // Won't use these here, as price should be set by own entry module instead of signal
  // Signal should only care if it's ready to buy or not
  //  virtual bool      CheckOpenLong(double &price,double &sl,double &tp,datetime &expiration);
  //  virtual bool      CheckOpenShort(double &price,double &sl,double &tp,datetime &expiration);

  // TODO: Implement DELETE order check if active order if signal can provide info on this
  //  virtual bool    CheckDeleteLong(double &price);
  //  virtual bool    CheckDeleteShort(double &price);
};

ChangeAbleSignal::ChangeAbleSignal(/* args */)
    : m_indicator_name(indicator_name),
      m_indicator_check_type(indicator_check_type),
      m_input_value_0(input_value_0),
      m_input_value_1(input_value_1),
      m_input_value_2(input_value_2),
      m_input_value_3(input_value_3),
      m_input_value_4(input_value_4),
      m_input_value_5(input_value_5),
      m_input_value_6(input_value_6),
      m_input_value_7(input_value_7),
      m_input_value_8(input_value_8),
      m_input_value_9(input_value_9),
      m_input_type_0(input_type_0),
      m_input_type_1(input_type_1),
      m_input_type_2(input_type_2),
      m_input_type_3(input_type_3),
      m_input_type_4(input_type_4),
      m_input_type_5(input_type_5),
      m_input_type_6(input_type_6),
      m_input_type_7(input_type_7),
      m_input_type_8(input_type_8),
      m_input_type_9(input_type_9),
      m_upper_check_val(upper_check_val),
      m_lower_check_val(lower_check_val),
      m_check_buffer_0(check_buffer_0),
      m_check_buffer_1(check_buffer_1),
      m_check_buffer_2(check_buffer_2),
      m_check_buffer_3(check_buffer_3),
      m_check_idx(check_idx),
      m_result(100) {
  m_used_series = USE_SERIES_OPEN + USE_SERIES_HIGH + USE_SERIES_LOW + USE_SERIES_CLOSE + USE_SERIES_TIME;
}

ChangeAbleSignal::~ChangeAbleSignal() {}

bool ChangeAbleSignal::ValidationSettings(void) {
  if (!CExpertSignal::ValidationSettings()) return false;

  if (m_check_buffer_0 == -1) {
    printf(__FUNCTION__ + ": index value for buffer 0 must be provided");
    return false;
  }
  if (m_indicator_check_type == INDICATOR_CHECK_CROSSOVER) {
    if ((m_check_buffer_0 == -1) || (m_check_buffer_1 == -1)) {
      printf(__FUNCTION__ + ": index value for buffer 0 AND 1 must be provided");
      return false;
    }
  }
  if (m_indicator_check_type == INDICATOR_CHECK_CROSSOVER_0_1__2_3) {
    if ((m_check_buffer_0 == -1) || (m_check_buffer_1 == -1) || (m_check_buffer_2 == -1) || (m_check_buffer_3 == -1)) {
      printf(__FUNCTION__ + ": index value for buffer 0 AND 1 must be provided");
      return false;
    }
  }

  return true;
}

void ChangeAbleSignal::BuildParams(void) {
  // Count param length
  int total_params = 1; // Initially 1, as string also has a type
  if (m_input_type_0 != TYPE_CHAR) { total_params++; };
  if (m_input_type_1 != TYPE_CHAR) { total_params++; };
  if (m_input_type_2 != TYPE_CHAR) { total_params++; };
  if (m_input_type_3 != TYPE_CHAR) { total_params++; };
  if (m_input_type_4 != TYPE_CHAR) { total_params++; };
  if (m_input_type_5 != TYPE_CHAR) { total_params++; };
  if (m_input_type_6 != TYPE_CHAR) { total_params++; };
  if (m_input_type_7 != TYPE_CHAR) { total_params++; };
  if (m_input_type_8 != TYPE_CHAR) { total_params++; };
  if (m_input_type_9 != TYPE_CHAR) { total_params++; };
  // Resize to number of actually used parameters
  ArrayResize(mql_param, total_params);

  mql_param[0].type          = TYPE_STRING;
  mql_param[0].string_value  = "" + m_indicator_name + "";
  if (m_input_type_0 != TYPE_CHAR) { setMqlParam(mql_param[1], m_input_value_0, m_input_type_0); };
  if (m_input_type_1 != TYPE_CHAR) { setMqlParam(mql_param[2], m_input_value_1, m_input_type_1); };
  if (m_input_type_2 != TYPE_CHAR) { setMqlParam(mql_param[3], m_input_value_2, m_input_type_2); };
  if (m_input_type_3 != TYPE_CHAR) { setMqlParam(mql_param[4], m_input_value_3, m_input_type_3); };
  if (m_input_type_4 != TYPE_CHAR) { setMqlParam(mql_param[5], m_input_value_4, m_input_type_4); };
  if (m_input_type_5 != TYPE_CHAR) { setMqlParam(mql_param[6], m_input_value_5, m_input_type_5); };
  if (m_input_type_6 != TYPE_CHAR) { setMqlParam(mql_param[7], m_input_value_6, m_input_type_6); };
  if (m_input_type_7 != TYPE_CHAR) { setMqlParam(mql_param[8], m_input_value_7, m_input_type_7); };
  if (m_input_type_8 != TYPE_CHAR) { setMqlParam(mql_param[9], m_input_value_8, m_input_type_8); };
  if (m_input_type_9 != TYPE_CHAR) { setMqlParam(mql_param[10], m_input_value_9, m_input_type_9); };
}

void ChangeAbleSignal::setMqlParam(MqlParam &p, double value, ENUM_DATATYPE type) {
  p.type = type;
  switch (type) {
    case TYPE_UINT:
    case TYPE_ULONG:
    case TYPE_UCHAR:
      p.integer_value = (uint)value;
      break;
    case TYPE_BOOL:
    case TYPE_CHAR:
    case TYPE_SHORT:
    case TYPE_USHORT:
    case TYPE_INT:
    case TYPE_LONG:
    case TYPE_COLOR:
    case TYPE_DATETIME:
      p.integer_value = (int)value;
      break;
    case TYPE_FLOAT:
    case TYPE_DOUBLE:
      p.double_value = value;
      break;
  }
}

bool ChangeAbleSignal::InitIndicators(CIndicators *indicators) {
  //--- check pointer
  if (indicators == NULL) {
    printf(__FUNCTION__ + ": Pointer to indicator is NULL!");
    return false;
  }

  //--- initialization of indicators and timeseries of additional filters
  if (!CExpertSignal::InitIndicators(indicators)) return false;

  //--- add object to collection
  if (!indicators.Add(GetPointer(m_custom_ind))) {
    printf(__FUNCTION__ + ": error adding object (  )");
    return false;
  }

  // Build parameters
  BuildParams();

  // Init indicator
  if (!m_custom_ind.Create(m_symbol.Name(), m_period, IND_CUSTOM, ArraySize(mql_param), mql_param)) {
    printf(__FUNCTION__ + ": error initializing object (m_custom_ind)");
    return false;
  }

  // Add indicator to chart. This should use a new window if needed by the indicator
  if (!ChartIndicatorAdd(0, 0, m_custom_ind.Handle())) {
    Print("Failed to add Indicator: ", m_indicator_name);
    Print("Error code ", GetLastError());
    return false;
  }
  return true;
}

// For info on long/short conditions: https://www.mql5.com/en/code/267

int ChangeAbleSignal::LongCondition(void) {
  int result = 0;
  // Last full candle
  // This returns previous closed candle, we want current candle that just closed.
  // int idx    = StartIndex();
  // int digits = 6;
  // double indicator_val = 0.0;
  if (!newBarLong()) {
    return 0;
  }

  switch (indicator_check_type) {
    case INDICATOR_CHECK_CROSSOVER:
      result = LongConditionCrossover(m_check_buffer_0, m_check_buffer_1);
      break;
    case INDICATOR_CHECK_CROSSOVER_0_1__2_3:
      result = LongConditionCrossover(m_check_buffer_0, m_check_buffer_1);
      break;
    case INDICATOR_CHECK_UPPER_LOWER_0:
      result = LongConditionAbove(m_check_buffer_0, m_upper_check_val);
      break;
    // NOT possible unless also checking trend direction of indicator!
    // case INDICATOR_CHECK_ABOVE_LOWER_0:
    // result = LongConditionAboveUpper(m_check_buffer_0);
    // break;
    // case INDICATOR_CHECK_BELOW_UPPER_1:
    // break;
    case INDICATOR_CHECK_BETWEEN_CORRECT_TREND:
      result = LongConditionBetweenCorrectTrend(m_check_buffer_0);
      break;
    case INDICATOR_CHECK_PRICE_CLOSE:
      result = LongConditionPriceAboveIndicator(m_check_buffer_0);
      break;
    case INDICATOR_CHECK_OPPOSITE_PRICE_CLOSE:
      result = LongConditionPriceBelowIndicator(m_check_buffer_0);
      break;
    case INDICATOR_CHECK_PRICE_CLOSE_0_1:
      result = LongConditionPriceAboveIndicator(m_check_buffer_0);
      break;
    case INDICATOR_CHECK_PRICE_CLOSE_SWITCHED:
      if (ShortConditionPriceBelowIndicator(m_check_buffer_0, 1) > 0) {
        result = LongConditionPriceAboveIndicator(m_check_buffer_0);
      }
      break;
    case INDICATOR_CHECK_PRICE_CLOSE_0_1_SWITCHED:
      if (ShortConditionPriceBelowIndicator(m_check_buffer_1, 1) > 0) {
        result = LongConditionPriceAboveIndicator(m_check_buffer_0);
      }
      break;
    case INDICATOR_CHECK_ZERO_LINE_CROSS:
      result = LongConditionCrossLevel(m_check_buffer_0, 0);
      break;
    case INDICATOR_CHECK_CROSS_LEVEL:
      result = LongConditionCrossLevel(m_check_buffer_0, m_upper_check_val);
      break;
    case INDICATOR_CHECK_CROSS_LEVEL_0_1:
      result = LongConditionCrossLevel(m_check_buffer_0, m_upper_check_val);
      break;
    case INDICATOR_CHECK_CROSS_LEVEL_0_1_EMPTY_VALUE:
      result = LongConditionCrossLevelOrEmptyValue(m_check_buffer_0, m_upper_check_val);
      break;
    case INDICATOR_CHECK_SLOPE_BARS:
      result = LongConditionSlope(m_check_buffer_0, (int)m_upper_check_val);
      break;
    case INDICATOR_CHECK_SLOPE_CHANGED_BARS:
      result = LongConditionSlopeChanged(m_check_buffer_0, (int)m_upper_check_val);
      break;
  }
  return result;
}

int ChangeAbleSignal::ShortCondition(void) {
  int result = 0;
  // Last full candle
  // This returns previous closed candle, we want current candle that just closed.
  // int idx    = StartIndex();
  // int digits = 6;
  if (!newBarShort()) {
    return 0;
  }

  switch (indicator_check_type) {
    case INDICATOR_CHECK_CROSSOVER:
      result = ShortConditionCrossover(m_check_buffer_0, m_check_buffer_1);
      break;
    case INDICATOR_CHECK_CROSSOVER_0_1__2_3:
      result = ShortConditionCrossover(m_check_buffer_2, m_check_buffer_3);
      break;
    case INDICATOR_CHECK_UPPER_LOWER_0:
      result = ShortConditionBelow(m_check_buffer_0, m_lower_check_val);
      break;
    // case INDICATOR_CHECK_ABOVE_LOWER_0:
    // result = ShortConditionBelowLower(m_check_buffer_1);
    // break;
    // case INDICATOR_CHECK_BELOW_UPPER_1:
    // break;
    case INDICATOR_CHECK_BETWEEN_CORRECT_TREND:
      result = ShortConditionBetweenCorrectTrend(m_check_buffer_0);
      break;
    case INDICATOR_CHECK_PRICE_CLOSE:
      result = ShortConditionPriceBelowIndicator(m_check_buffer_0);
      break;
    case INDICATOR_CHECK_PRICE_CLOSE_0_1:
      result = ShortConditionPriceBelowIndicator(m_check_buffer_1);
      break;
    case INDICATOR_CHECK_OPPOSITE_PRICE_CLOSE:
      result = ShortConditionPriceAboveIndicator(m_check_buffer_0);
      break;
    case INDICATOR_CHECK_PRICE_CLOSE_SWITCHED:
      if (LongConditionPriceAboveIndicator(m_check_buffer_0, 1) > 0) {
        result = ShortConditionPriceBelowIndicator(m_check_buffer_0);
      }
      break;
    case INDICATOR_CHECK_PRICE_CLOSE_0_1_SWITCHED:
      if (LongConditionPriceAboveIndicator(m_check_buffer_0, 1) > 0) {
        result = ShortConditionPriceBelowIndicator(m_check_buffer_1);
      }
      break;
    case INDICATOR_CHECK_ZERO_LINE_CROSS:
      result = ShortConditionCrossLevel(m_check_buffer_0, 0);
      break;
    case INDICATOR_CHECK_CROSS_LEVEL:
      result = ShortConditionCrossLevel(m_check_buffer_0, m_lower_check_val);
      break;
    case INDICATOR_CHECK_CROSS_LEVEL_0_1:
      result = ShortConditionCrossLevel(m_check_buffer_1, m_lower_check_val);
      break;
    case INDICATOR_CHECK_CROSS_LEVEL_0_1_EMPTY_VALUE:
      result = ShortConditionCrossLevelOrEmptyValue(m_check_buffer_1, m_lower_check_val);
      break;
    case INDICATOR_CHECK_SLOPE_BARS:
      result = ShortConditionSlope(m_check_buffer_0, (int)m_upper_check_val);
      break;
    case INDICATOR_CHECK_SLOPE_CHANGED_BARS:
      result = ShortConditionSlopeChanged(m_check_buffer_0, (int)m_upper_check_val);
      break;
  }
  return result;
}

// Check crossover on +1 and +2, since check index is current first tick on new candle!
int ChangeAbleSignal::LongConditionCrossover(int buffer_1, int buffer_2) {
  // Just ensure buffer 2 is HIGHER than buffer 1. Actual crossover point is not critical when backtesting indicator
  // This can be improved by not storing values.
  uint offset_idx = checkIndex();
  double val_1   = m_custom_ind.GetData(buffer_1, offset_idx);
  double val_2   = m_custom_ind.GetData(buffer_2, offset_idx);
  double val_1_1 = m_custom_ind.GetData(buffer_1, offset_idx + 1);
  double val_2_1 = m_custom_ind.GetData(buffer_2, offset_idx + 1);
  // Note: For system with following NNFX, the 'LONG' signal should be held for 7 candles or until This indicator reverses
  // For testing, it does not matter, as trade will be entered on crossover.
  // Also check against previous candle, that it was a valid crossover!
  if (val_1 > val_2) {
    if (val_1_1 < val_2_1) {
      return m_result;
    }
  }
  return 0;
}

// Check crossover on +1 and +2, since check index is current first tick on new candle!
int ChangeAbleSignal::ShortConditionCrossover(int buffer_1, int buffer_2) {
  // Just ensure buffer 2 is LOWER than buffer 1. Actual crossover point is not critical when backtesting indicator
  uint offset_idx = checkIndex();
  double val_1   = m_custom_ind.GetData(buffer_1, offset_idx);
  double val_2   = m_custom_ind.GetData(buffer_2, offset_idx);
  double val_1_1 = m_custom_ind.GetData(buffer_1, offset_idx + 1);
  double val_2_1 = m_custom_ind.GetData(buffer_2, offset_idx + 1);
  if (val_1 < val_2) {
    if (val_1_1 > val_2_1) {
      return m_result;
    }
  }
  return 0;
}

int ChangeAbleSignal::LongConditionAbove(int buffer, double threshold_value) {
  double val = m_custom_ind.GetData(buffer, checkIndex());
  if (val > threshold_value) {
    return m_result;
  }
  return 0;
}
int ChangeAbleSignal::ShortConditionBelow(int buffer, double threshold_value) {
  double val = m_custom_ind.GetData(buffer, checkIndex());
  if (val < threshold_value) {
    return m_result;
  }
  return 0;
}

int ChangeAbleSignal::LongConditionBetweenCorrectTrend(int buffer) {
  double val = m_custom_ind.GetData(buffer, checkIndex());
  if ((val > m_lower_check_val) && (val < m_upper_check_val)) {
    // Trend correct over last two buffer idx's
    if (val > m_custom_ind.GetData(buffer, checkIndex() + 1)) {
      return m_result;
    }
  }
  return 0;
}

int ChangeAbleSignal::ShortConditionBetweenCorrectTrend(int buffer) {
  double val = m_custom_ind.GetData(buffer, checkIndex());
  if ((val > m_lower_check_val) && (val < m_upper_check_val)) {
    // Trend correct over last two buffer idx's
    if (val < m_custom_ind.GetData(buffer, checkIndex() + 1)) {
      return m_result;
    }
  }
  return 0;
}

int ChangeAbleSignal::LongConditionPriceAboveIndicator(int buffer, int offset = 0) {
  // int otheridx = StartIndex();
  double val   = m_custom_ind.GetData(buffer, checkIndex() + offset);
  // Return 0 if value is 0 or 'EMPTY_VALUE'
  if ((val == 0) || (val == EMPTY_VALUE)) {
    return 0;
  }
  double price = Close(m_check_idx + offset);
  if (price > val) {
    return m_result;
  }
  return 0;
}

int ChangeAbleSignal::LongConditionPriceBelowIndicator(int buffer, int offset = 0) {
  // int otheridx = StartIndex();
  double val   = m_custom_ind.GetData(buffer, checkIndex() + offset);
  // Return 0 if value is 0 or 'EMPTY_VALUE'
  if ((val == 0) || (val == EMPTY_VALUE)) {
    return 0;
  }
  double price = Close(m_check_idx + offset);
  if (price < val) {
    return m_result;
  }
  return 0;
}

int ChangeAbleSignal::ShortConditionPriceAboveIndicator(int buffer, int offset = 0) {
  // int otheridx = StartIndex();
  double val   = m_custom_ind.GetData(buffer, checkIndex() + offset);
  // Return 0 if value is 0 or 'EMPTY_VALUE'
  if ((val == 0) || (val == EMPTY_VALUE)) {
    return 0;
  }
  double price = Close(m_check_idx + offset);
  if (price > val) {
    return m_result;
  }
  return 0;
}

int ChangeAbleSignal::ShortConditionPriceBelowIndicator(int buffer, int offset = 0) {
  // int otheridx = StartIndex();
  double val   = m_custom_ind.GetData(buffer, checkIndex() + offset);
  // Return 0 if value is 0 or 'EMPTY_VALUE'
  if ((val == 0) || (val == EMPTY_VALUE)) {
    return 0;
  }
  double price = Close(m_check_idx + offset);
  if (price < val) {
    return m_result;
  }
  return 0;
}

int ChangeAbleSignal::LongConditionCrossLevel(int buffer, double level) {
  double val   = m_custom_ind.GetData(buffer, checkIndex());
  double val_1 = m_custom_ind.GetData(buffer, checkIndex() + 1);
  if (level < val) {
    if (level > val_1) {
      return m_result;
    }
  }
  return 0;
}

int ChangeAbleSignal::LongConditionCrossLevelOrEmptyValue(int buffer, double level) {
  double val   = m_custom_ind.GetData(buffer, checkIndex());
  double val_1 = m_custom_ind.GetData(buffer, checkIndex() + 1);
  if ((level < val) && (val != EMPTY_VALUE)) {
    if ((level > val_1) || (val_1 == EMPTY_VALUE)) {
      return m_result;
    }
  }
  return 0;
}

int ChangeAbleSignal::ShortConditionCrossLevel(int buffer, double level) {
  double val   = m_custom_ind.GetData(buffer, checkIndex());
  double val_1 = m_custom_ind.GetData(buffer, checkIndex() + 1);
  if (level > val) {
    if (level < val_1) {
      return m_result;
    }
  }
  return 0;
}

int ChangeAbleSignal::ShortConditionCrossLevelOrEmptyValue(int buffer, double level) {
  double val   = m_custom_ind.GetData(buffer, checkIndex());
  double val_1 = m_custom_ind.GetData(buffer, checkIndex() + 1);
  if ((level > val) && (val != EMPTY_VALUE)) {
    if ((level < val_1) || (val_1 == EMPTY_VALUE)) {
      return m_result;
    }
  }
  return 0;
}

int ChangeAbleSignal::LongConditionSlope(int buffer, int bars) {
  int direction_count = 0;
  double val          = m_custom_ind.GetData(buffer, checkIndex());
  double val_1        = m_custom_ind.GetData(buffer, checkIndex() + 1);
  for (int i = 0; i < bars; i++) {
    val   = m_custom_ind.GetData(buffer, checkIndex() + i);
    val_1 = m_custom_ind.GetData(buffer, checkIndex() + i + 1);
    if (val > val_1) {
      direction_count += 1;
    }
  }

  if (direction_count == bars) {
    return m_result;
  }
  return 0;
}

int ChangeAbleSignal::ShortConditionSlope(int buffer, int bars) {
  int direction_count = 0;
  double val          = m_custom_ind.GetData(buffer, checkIndex());
  double val_1        = m_custom_ind.GetData(buffer, checkIndex() + 1);
  for (int i = 0; i < bars; i++) {
    val   = m_custom_ind.GetData(buffer, checkIndex() + i);
    val_1 = m_custom_ind.GetData(buffer, checkIndex() + i + 1);
    if (val < val_1) {
      direction_count += 1;
    }
  }

  if (direction_count == bars) {
    return m_result;
  }
  return 0;
}

int ChangeAbleSignal::LongConditionSlopeChanged(int buffer, int bars) {
  int direction_count = 0;
  double val          = m_custom_ind.GetData(buffer, checkIndex());
  double val_1        = m_custom_ind.GetData(buffer, checkIndex() + 1);
  for (int i = 0; i < bars; i++) {
    val   = m_custom_ind.GetData(buffer, checkIndex() + i);
    val_1 = m_custom_ind.GetData(buffer, checkIndex() + i + 1);
    if (val > val_1) {
      direction_count += 1;
    }
  }
  // Check that we had a change before the bar count (accept 1 bar change)
  val   = m_custom_ind.GetData(buffer, checkIndex() + bars);
  val_1 = m_custom_ind.GetData(buffer, checkIndex() + bars + 1);

  if ((direction_count == bars) && (val < val_1)) {
    return m_result;
  }
  return 0;
}

int ChangeAbleSignal::ShortConditionSlopeChanged(int buffer, int bars) {
  int direction_count = 0;
  double val          = m_custom_ind.GetData(buffer, checkIndex());
  double val_1        = m_custom_ind.GetData(buffer, checkIndex() + 1);
  for (int i = 0; i < bars; i++) {
    val   = m_custom_ind.GetData(buffer, checkIndex() + i);
    val_1 = m_custom_ind.GetData(buffer, checkIndex() + i + 1);
    if (val < val_1) {
      direction_count += 1;
    }
  }
  // Check that we had a change before the bar count (accept 1 bar change)
  val   = m_custom_ind.GetData(buffer, checkIndex() + bars);
  val_1 = m_custom_ind.GetData(buffer, checkIndex() + bars + 1);

  if ((direction_count == bars) && (val > val_1)) {
    return m_result;
  }
  return 0;
}

double ChangeAbleSignal::Direction(void) {
  // long mask;
  // double direction;
  double main_direction = 0.0;
  double filter_result  = 0.0;
  int filter_count      = 0;  // number of "voted" filters

  m_custom_ind.Refresh();

  // Ignore weight when testing
  // main_direction = m_weight * (LongCondition() - ShortCondition());
  main_direction = LongCondition() - ShortCondition();
  setDirectionalComment("Changeable test signal", main_direction, COMMENT_TEST_SIGNAL);

  //--- return the result
  return (main_direction);
}
