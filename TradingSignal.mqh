//+------------------------------------------------------------------+
//|                                        TradingSignal.mqh         |
//|                        Copyright 2019-2021, Leif Ringstad        |
//+------------------------------------------------------------------+

// This should be the 'base' trading signal, which
// gets info on either long or short from it's filters
// This should handle the price, TP and SL levels
//

#include "defines.mqh"
#include "SignalBase.mqh"

/* Inputs */
// These should probable be moved OUT of signal, and back to main/ea
input uint i_ATR_period          = 14; // ATR period for SL/TP
input double i_ATR_SL_MULTIPLIER   = 1.5;  // ATR Stop loss multiplier
input double i_ATR_TP_MULTIPLIER = 1;  // ATR TP multiplier
input bool i_USE_bid_ask = true; // Use bid/ask instead of close when settings sl/tp
class TradingSignal : public SignalBase {
 protected:
  CArrayObj m_trend_filters;  // array of dual direction trend filters (maximum number of filters is 64)

   //--- method of initialization of the object
   void              Magic(ulong value);

 private:
  /* data */

  int m_result;

  double m_ATR_SL_MULTIPLIER;
  double m_ATR_TP_MULTIPLIER;
  uint m_ATR_period;
  bool m_USE_bid_ask;

  /* ATR - calculate SL/TP levels */
  CiATR m_atr;

 public:
  TradingSignal(/* args */);
  ~TradingSignal();

  double ATR_SL() { return m_ATR_SL_MULTIPLIER; };
  double ATR_TP() { return m_ATR_TP_MULTIPLIER; };

  //--- method of verification of settings
  virtual bool ValidationSettings(void);
  //--- method of creating the indicator and timeseries
  virtual bool InitIndicators(CIndicators *indicators);

  //--- methods of checking if the market models are formed
  int UsedSeries(void);

  // These should be implemented when checks for volatility is required
  virtual int LongCondition(void) { return (1); }
  virtual int ShortCondition(void) { return (1); }

  virtual double Direction(void);

  //--- methods for generating signals of entering the market
  // Signal should only care if it's ready to buy or not
  // virtual bool      CheckOpenLong(double &price,double &sl,double &tp,datetime &expiration);
  // virtual bool      CheckOpenShort(double &price,double &sl,double &tp,datetime &expiration);

  // Implement these if overriding base implementation!
  //--- methods for detection of levels of entering the market
  virtual bool OpenLongParams(double &price, double &sl, double &tp, datetime &expiration);
  virtual bool OpenLongParamsTP(double &price, double &sl, double &tp, datetime &expiration);
  virtual bool OpenShortParams(double &price, double &sl, double &tp, datetime &expiration);
  virtual bool OpenShortParamsTP(double &price, double &sl, double &tp, datetime &expiration);

  void setPrices(double direction);

  void setLongPrices(int trigger_idx);
  void LongPositionEntryPrice(int trigger_idx);
  void LongPositionSLPrice(int trigger_idx);
  void LongPositionTPPrice(int trigger_idx);
//  void LongPositionTP2Price(int trigger_idx);

  void setShortPrices(int trigger_idx);
  void ShortPositionEntryPrice(int trigger_idx);
  void ShortPositionSLPrice(int trigger_idx);
  void ShortPositionTPPrice(int trigger_idx);
//  void ShortPositionTP2Price(int trigger_idx);

  private:
  double getATRDiff(double multiplier);
};

TradingSignal::TradingSignal(/* args */)
    : m_ATR_SL_MULTIPLIER(i_ATR_SL_MULTIPLIER),
      m_ATR_TP_MULTIPLIER(i_ATR_TP_MULTIPLIER),
      m_ATR_period(i_ATR_period),
      m_USE_bid_ask(i_USE_bid_ask),
      m_result(1) {
  m_used_series = USE_SERIES_OPEN + USE_SERIES_HIGH + USE_SERIES_LOW + USE_SERIES_CLOSE;  // + USE_SERIES_TIME;
}

TradingSignal::~TradingSignal() {}

bool TradingSignal::InitIndicators(CIndicators *indicators) {
  if (indicators == NULL) {
    printf(__FUNCTION__ + ": Pointer to indicator is NULL!");
    return false;
  }

  if (!CExpertSignal::InitIndicators(indicators)) return false;

  //--- add ATR
  if (!indicators.Add(GetPointer(m_atr)))
  {
    printf(__FUNCTION__ + ": error adding object (m_atr)");
    return false;
  }

  // Init alligator
  if (!m_atr.Create(m_symbol.Name(), m_period, m_ATR_period)) {
    printf(__FUNCTION__ + ": error initializing object (m_atr)");
    return false;
  }

  // Add atr to chart if debugging!
  // ChartIndicatorAdd(0,0, m_atr.Handle());

  // Initialize filters (indicators to test)
  //---
  // THIS IS DONE IN CEXPERTBASE!
//   CExpertSignal *filter;
//   int            total=m_filters.Total();
  //--- gather information about using of timeseries
//   for(int i=0;i<total;i++)
//     {
//      filter=m_filters.At(i);
//      m_used_series|=filter.UsedSeries();
//     }

  //---
   CExpertSignal *trend_filter;
   int trend_total=m_trend_filters.Total();
  //--- gather information about using of timeseries
   for(int i=0;i<trend_total;i++)
     {
      trend_filter=m_trend_filters.At(i);
      m_used_series|=trend_filter.UsedSeries();
     }

//--- create required timeseries
   if(!CExpertBase::InitIndicators(indicators))
      return(false);
//--- initialization of indicators and timeseries in the additional filters
// THIS IS ALREADY DONE IN EXPERTBASE!
//   for(int i=0;i<total;i++)
//     {
//      filter=m_filters.At(i);
//      filter.SetPriceSeries(m_open,m_high,m_low,m_close);
//      filter.SetOtherSeries(m_spread,m_time,m_tick_volume,m_real_volume);
//      if(!filter.InitIndicators(indicators))
//         return(false);
//     }

//--- initialization of indicators and timeseries in the additional filters
   for(int i=0;i<trend_total;i++)
     {
      trend_filter=m_trend_filters.At(i);
      trend_filter.SetPriceSeries(m_open,m_high,m_low,m_close);
      trend_filter.SetOtherSeries(m_spread,m_time,m_tick_volume,m_real_volume);
      if(!trend_filter.InitIndicators(indicators))
         return(false);
     }

//--- succeed
   return(true);
}

double TradingSignal::getATRDiff(double multiplier) {
  // Get atr from previous candle, as per NNFX guides?
  // return(m_atr.Main(1) * multiplier);
  // Or use current candle ATR?
  return(m_atr.Main(1) * multiplier);
  // return 0;
}

// int TradingSignal::LongCondition(void) {
//     int result= 0;
//     return result;
// }

// int TradingSignal::ShortCondition(void) {
//     int result= 0;
//     return result;
// }

//+------------------------------------------------------------------+
//| Sets magic number for object and its dependent objects           |
//+------------------------------------------------------------------+
void TradingSignal::Magic(ulong value) {
  int total = m_trend_filters.Total();
  //--- loop by the additional trend filters
  for (int i = 0; i < total; i++) {
    CExpertSignal *trend_filter = m_trend_filters.At(i);
    //--- check pointer
    if (trend_filter == NULL) continue;
    trend_filter.Magic(value);
  }

  CExpertSignal::Magic(value);
}

int TradingSignal::UsedSeries(void) {
  if (m_other_symbol || m_other_period) return (1);

  m_used_series = CExpertSignal::UsedSeries();
  int total     = m_trend_filters.Total();
  //--- loop by the additional trend filters
  for (int i = 0; i < total; i++) {
    CExpertSignal *trend_filter = m_trend_filters.At(i);
    //--- check pointer
    if (trend_filter == NULL) return (false);
    m_used_series |= trend_filter.UsedSeries();
  }

  //---
  return (m_used_series);
}

bool TradingSignal::ValidationSettings(void)
  {
   if(!CExpertSignal::ValidationSettings())
      return(false);
//--- check of parameters in the additional filters
   int total = m_trend_filters.Total();
//--- loop by the additional filters
   for(int i=0;i<total;i++)
     {
      CExpertSignal *filter=m_trend_filters.At(i);
      //--- check pointer
      if(filter==NULL)
         return(false);
      if(!filter.ValidationSettings())
         return(false);
     }
//--- succeed
   return(true);
  }

//+------------------------------------------------------------------+
//| Add additional trend filter                                      |
//+------------------------------------------------------------------+
// bool TradingSignal::AddTrendFilter(TrendSignalBase *trend_filter) {
//   //--- check pointer
//   if (trend_filter == NULL)
//     return (false);
//   //--- primary initialization of the filter
//   if (!trend_filter.Init(m_symbol, m_period, m_adjusted_point))
//     return (false);
//   //--- add the filter to the array of filters
//   if (!m_trend_filters.Add(trend_filter))
//     return (false);
//   trend_filter.EveryTick(m_every_tick);
//   trend_filter.Magic(m_magic);
//   //--- succeed
//   return (true);
// }

double TradingSignal::Direction(void) {
  long mask;
  double direction;
  // double trend_filter_val;
  double main_direction         = 0.0;
  double indicator_total_result = 0.0;
  double trend_filter_total_result = 0.0;
  int indicator_count           = 0;  // number of "voted" filters
  int trend_filter_count        = 0;  // number of "voted" filters

  // Go through filters to get short/long sentiment
  //---
  int total = m_filters.Total();

  //--- loop by filters
  // Keep maskin if used later!
  for (int i = 0; i < total; i++) {
    //--- mask for bit maps
    mask = ((long)1) << i;
    //--- check of the flag of ignoring the signal of filter
    if ((m_ignore & mask) != 0) continue;
    CExpertSignal *filter = m_filters.At(i);
    //--- check pointer
    if (filter == NULL) continue;
    direction = filter.Direction();
    // Keep?
    //--- the "prohibition" signal
    // if(direction==EMPTY_VALUE)
    //   return(EMPTY_VALUE);
    //--- check of flag of inverting the signal of filter
    // if((m_invert&mask)!=0)
    //   filter_result-=direction;
    // else
    indicator_total_result += direction;
    indicator_count++;
  }

  // Return 0 if there are no indicators.
  if (indicator_count == 0) return 0.0;

  //--- normalization
  indicator_total_result /= indicator_count;

  // // Check any running trend filers
  // total = m_trend_filters.Total();

  // //--- loop by filters
  // // Keep maskin if used later!
  // if (total > 0) {
  //   for (int i = 0; i < total; i++) {
  //     trend_filter_count++;
  //     TrendSignalBase *trend_base = m_trend_filters.At(i);
  //     trend_filter_val = trend_base.TrendConfirmation();
  //   }
  //   trend_filter_total_result /= trend_filter_count;
  // }
  // else {
  //   // No trend filters, so assume we don't use them
  //   trend_filter_total_result = m_threshold_open;
  // }


  if ((indicator_total_result >= m_threshold_open) && (trend_filter_total_result >= m_threshold_open)) {
    // Trend wants LONG trades only
    Print("Test signal says: LONG\n");
    // main_direction = LongCondition();
  } else if ((indicator_total_result <= -m_threshold_open) && (trend_filter_total_result >= m_threshold_open)) {
    // Trend wants SHORT trades only
    Print("Test signal says: SHORT\n");
    // main_direction = -ShortCondition();
  }

  // Set prices
  setPrices(indicator_total_result);

  //--- return the result
  return (indicator_total_result);
}

void TradingSignal::setPrices(double direction) {
  if (direction > 0) {
    setLongPrices(StartIndex());
  } else if (direction < 0) {
    setShortPrices(StartIndex());
  }
}

void TradingSignal::setLongPrices(int trigger_idx) {
  LongPositionEntryPrice(trigger_idx);
  LongPositionSLPrice(trigger_idx);
  LongPositionTPPrice(trigger_idx);
  // LongPositionTP2Price(trigger_idx);
  // double open0 = Open(1);
  // double open1 = Open(1);
  // double close0 = Close(1);
  // double close1 = Close(1);
  // double atr0 = m_atr.Main(1);
  // double atr1 = m_atr.Main(1);
  // datetime time0 = Time(1);
  // datetime time1 = Time(1);
  string debug_comment = StringFormat("DEBUG: TradingSignal Long Position: \n" +
                "Open: %s\n" +
                "Close: %s\n" +
                "ATR: %s\n" +
                "Time: %s\n",
                DoubleToString(Open(1),5),
                DoubleToString(Close(1),5),
                DoubleToString(m_atr.Main(1),5),
                TimeToString(Time(1))
              );
  setComment(debug_comment, COMMENT_MAIN_SIGNAL_DEBUG);
}

void TradingSignal::LongPositionEntryPrice(int trigger_idx) {
  m_base_price = 0.0; // Set to 0, to do instant buy
  // Close 0 gives current open candle, while close(1) gives previous fully closed candle
  // m_base_price = Close(1); // Perhaps add some pips here to delay ?
}

// Use ATR for levels, as suggested by VP/NNFX
void TradingSignal::LongPositionSLPrice(int trigger_idx) {
  // Distance from entry to stop level should be given here ? or direct price? Need to test!
  double Bid = NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits); // Get the Bid Price
  if (m_USE_bid_ask) {
     m_sl_price= Bid - (m_atr.Main(1) * m_ATR_SL_MULTIPLIER);
  } else {
     m_sl_price = Close(1) - (m_atr.Main(1) * m_ATR_SL_MULTIPLIER);
  }

}

void TradingSignal::LongPositionTPPrice(int trigger_idx) {
  double Ask = NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits); // Get the Ask Price
  if (m_USE_bid_ask) {
     m_tp_price = Ask + (m_atr.Main(1) * m_ATR_TP_MULTIPLIER);
  } else {
     m_tp_price = Close(1) + (m_atr.Main(1) * m_ATR_TP_MULTIPLIER);
  }
}

// double TradingSignal::LongPositionTP2Price(int trigger_idx) {
//   // m_take_level_2 = m_stop_level*2;
// }

void TradingSignal::setShortPrices(int trigger_idx) {
  ShortPositionEntryPrice(trigger_idx);
  ShortPositionSLPrice(trigger_idx);
  ShortPositionTPPrice(trigger_idx);
  string debug_comment = StringFormat("DEBUG: TradingSignal Short Position: \n" +
                "Open: %s\n" +
                "Close: %s\n" +
                "ATR: %s\n" +
                "Time: %s\n",
                DoubleToString(Open(1),5),
                DoubleToString(Close(1),5),
                DoubleToString(m_atr.Main(1),5),
                TimeToString(Time(1))
              );
  setComment(debug_comment, COMMENT_MAIN_SIGNAL_DEBUG);
}

void TradingSignal::ShortPositionEntryPrice(int trigger_idx) {
  m_base_price = 0.0; // Set to 0, to do instant sell
  // m_base_price = Close(1); // Perhaps add some pips here to delay ?
}

// For now, fixed levels are used, based on ATR 
// (should consider other levels fractals, support/resistance, day open/close, weekly etc)
void TradingSignal::ShortPositionSLPrice(int trigger_idx) {
  // Distance from entry to stop level should be given here ? or direct price? Need to test!
  double Ask = NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits); // Get the Ask Price
  if (m_USE_bid_ask) {
     m_sl_price = Ask + (m_atr.Main(1) * m_ATR_SL_MULTIPLIER);
  } else {
     m_sl_price = Close(1) + (m_atr.Main(1) * m_ATR_SL_MULTIPLIER);
  }
}

void TradingSignal::ShortPositionTPPrice(int trigger_idx) {
  double Bid = NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits); // Get the Ask Price
  if (m_USE_bid_ask) {
     m_tp_price = Bid - (m_atr.Main(1) * m_ATR_TP_MULTIPLIER);
  } else {
     m_tp_price = Close(1) - (m_atr.Main(1) * m_ATR_TP_MULTIPLIER);
  }
}

bool TradingSignal::OpenLongParams(double &price, double &sl, double &tp, datetime &expiration) {
  return OpenLongParamsTP(price, sl, tp, expiration);
}

bool TradingSignal::OpenLongParamsTP(double &price, double &sl, double &tp, datetime &expiration) {
  // return false until ready to set proper prices!
  setLongPrices(StartIndex());
  price = m_symbol.NormalizePrice(m_base_price);
  sl    = m_symbol.NormalizePrice(m_sl_price);
  tp    = m_symbol.NormalizePrice(m_tp_price);
  expiration += m_expiration * PeriodSeconds(m_period);
  return true;
}

bool TradingSignal::OpenShortParams(double &price, double &sl, double &tp, datetime &expiration) {
  return OpenShortParamsTP(price, sl, tp, expiration);
}

bool TradingSignal::OpenShortParamsTP(double &price, double &sl, double &tp, datetime &expiration) {
  // return false until ready to set proper prices!
  setShortPrices(StartIndex());
  price = m_symbol.NormalizePrice(m_base_price);
  sl    = m_symbol.NormalizePrice(m_sl_price);
  tp    = m_symbol.NormalizePrice(m_tp_price);
  expiration += m_expiration * PeriodSeconds(m_period);
  return true;
}
