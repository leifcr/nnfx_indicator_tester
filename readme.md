# NNFX Signal tester

This is a tester for testing indicators, and getting winrate, profit, lots, number of trades, wins and a bit more exported into CSV format.
To use the signal tester, attach the EA, add settings and run. The output should be in:
```C:\Users\USERNAME\appdata\roaming\MetaQuotes\Terminal\Common\Files``` and is called NNFX_Indicator_Testing.csv

NOTE: This is a test project. It does not give any trading advice, help or other. You can only use it to backtest a single indicator on multiple pairs (or genetic) and get the data written to a csv file. Use that CSV file in excel or other to look at the results.

If you run tests on 100s of indicators, merge all the data, and evaluate.

When backtesting on multiple pairs (MarketwWatch in the tester), each pass will be one line in the CSV. For genetic testing, each pass will be one line in the CSV. The CSV file is appended to if it exists. However, too large CSV files can be difficult to manage.

## Code requirements
This is based on the built in trading library that comes with MT5. Place the files in your MQL5\Expert\nnfx_signal_tester or similar to compile. For running the ex5 directly, just use the ex5 file.

## Settings

| Input                                         | Usage                                              |
| ----------------------------                  |----------------------------------------------------|
| ATR Period for SL/TP                          | Period for calculation of STR |
| ATR TP Multiplier                             | Multiplier for TP (TP*ATR)   |
| ATR SL Multiplier                             | Multiplier for SL (SL*ATR)   |
| Use bid/ask prices                            | use bid/ask for setting tp/sl instead of close price   |
| Indicator_name                                | this is the indicator to test. If your indicator is named 'ntrtr_asctrend.ex5' enter 'ntrtr_asctrend'. If the indicator is in a folder e.g. 'MyFolder/MyIndicator.ex5' enter 'MyFolder/MyIndicator'    |
| Indicator Check type                          | Crossover, above/below level etc. See description below |
| Changeind, Input value X                      | The value for parameter X of the indicator |
| Changeind, Input Type  X                      | Set to the correct type for the parameter |
| Changeind, upper check value                  | Value to check against 'upper' (see check types) |
| Changeind, lower check value                  | Value to check against 'lower' (see check types) |
| Changeind, check buffer X                     | Set to the buffer to check against |
| Index to check, 1 = Last fully closed candle  | Set to the required index of the candle you want to check the indicator against |
| Writing to CSV Report enabled or disabled     | true = enabled, false = disabled  |
| Document name                                 | Name of the csv file |
| Signal threshold open                         | NOT USED |
| Signal threshold close                        |  NOT USED |
| Max percentage loss on single trade           | If set, and ATR SL is more than percentage, percentage takes presedence. Usually set to 99% when testing indicators |
| Fixed volume for each trade                   | 0.01 (Default) |
| Allow multiple trades in same directions      | true = multiple long and/or short |
| Allow both short and long position at the same time | If an indicator might give short and long signals before closing, this is a good idea to enable |
| Exit on signal or TP/SL                       | true = close on opposite signal, false = only exit if TP or SL is hit |
| Document name                                 | not used / ignore |

For more info on settings/parameters, look at the code.

## Example settings

Example settings for indicator colorxxdpo.

| Input                                         | Value |
| ----------------------------                  |----------------------------------------------------|
| ATR Period for SL/TP                          | 14 |
| ATR TP Multiplier                             | 1.5 |
| ATR SL Multiplier                             | 1 |
| Use bid/ask prices                            | true |
| Indicator_name                                | colorxxdpo |
| Indicator Check type                          | Above 0 for long, blow 0 for short |
| Changeind, Input value 0                      | 7 |
| Changeind, Input value 1                      | 21 |
| Changeind, Input value 2                      | 15 |
| Changeind, Input value 3                      | 4 |
| Changeind, Input value 4                      | 5 |
| Changeind, Input value 5                      | 100 |
| Changeind, Input value 6                      | 1 |
| Changeind, Input value 7                      | 0 |
| Changeind, Input value 8                      | 0 |
| Changeind, Input value 9                      | 0 |
| Changeind, Input Type  0                      | int |
| Changeind, Input Type  1                      | int |
| Changeind, Input Type  2                      | int |
| Changeind, Input Type  3                      | int |
| Changeind, Input Type  4                      | int |
| Changeind, Input Type  5                      | int |
| Changeind, Input Type  6                      | int |
| Changeind, Input Type  7                      | char |
| Changeind, Input Type  8                      | char |
| Changeind, Input Type  9                      | char |
| Changeind, upper check value                  | 0 |
| Changeind, lower check value                  | 0 |
| Changeind, check buffer 0                     | 0 |
| Changeind, check buffer 1                     | -1 |
| Changeind, check buffer 2                     | -1 |
| Changeind, check buffer 3                     | -1 |
| Index to check, 1 = Last fully closed candle  | 1 |
| Writing to CSV Report enabled or disabled     | true |
| Document name                                 | nnfxsignal_tester |
| Signal threshold open                         | 100 |
| Signal threshold close                        | 100 |
| Max percentage loss on single trade           | 99 |
| Fixed volume for each trade                   | 0.01 |
| Allow multiple trades in same directions      | true |
| Allow both short and long position at the same time | true |
| Exit on signal or TP/SL                       | true |

## Indicator check types
These are the various check types implemented for the indicators. For more details, see ChangeableSignal.mqh

* Crossover for buffer 0 and 1'
* Crossover long buff 0,1, short buff 2,3'
* Ind val above upper - long, below lower - short, checks buffer 0'
* Ind val is between high and low, and trending in correct direction, checks buffer 0'
* BELOW price close - long, ABOVE price close - short (MA types)'
* Ind val is ABOVE close - long, and BELOW close - short (Supertrend/psar types)'
* SWITCHED - BELOW price close - long, ABOVE price close'
* Above 0 for long, below 0 for short'
* Above up level = long, below low level = short'
* BELOW (buf 0) close - long, ABOVE (buf 1) close - short'
* Slope direction for indicator in X bars (upper val)'
* Slope direction indicator in X bars AND change of slope (upper val)'
* Above up level = long (buf 0), below low level = short (buf 1)'
* SWITCHED BELOW (buf 0) close - long, ABOVE (buf 1) close - short'
* cross level buffer 0 1 + not empty value, Above up level = long (buf 0), below low level = short (buf 1)'


## License
See License (MIT License)

