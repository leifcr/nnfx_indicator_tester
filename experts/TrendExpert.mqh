//+------------------------------------------------------------------+
//|                                         TrendExpert.mqh          |
//|                        Copyright 2019-2021, Leif Ringstad        |
//+------------------------------------------------------------------+

#include <Expert/Expert.mqh>
#include "../SignalBase.mqh"
#include "../ExtendedPositionInfo.mqh"
class TrendExpert : public CExpert
{

public:
  bool Init(string symbol,ENUM_TIMEFRAMES period,bool every_tick,ulong magic=0);

  void setMainSignal(SignalBase *signal) { m_main_signal = signal; };

  virtual void OnTick(void);
  virtual bool Refresh(void);
  datetime ReporterStartTime() { return r_starttime; }
  datetime ReporterEndTime() { return r_endtime; }
  void setAllowHedge(bool allow_hedge) { m_allow_hedge = allow_hedge; }
  bool allowHedge(void) { return m_allow_hedge; }
  void setExitOnSignal(bool exit_on_signal) { m_exit_on_signal = exit_on_signal; }
  bool exitOnSignal(void) { return m_exit_on_signal; }
  void setAllowMultipleTrades(bool allow_multiple_trades) { m_allow_multiple_trades = allow_multiple_trades; }
  bool allowMultipleTrades(void) { return m_allow_multiple_trades; }

protected:
  virtual bool CheckOpen(void);
  virtual bool Processing(void);

  SignalBase *m_main_signal; // Same as m_signal, but more features
                                          // protected:
  bool m_allow_hedge;
  bool m_exit_on_signal;
  bool m_allow_multiple_trades;

private:
  datetime r_starttime;
  datetime r_endtime;
  ExtendedPositionInfo m_extended_positioninfo;
};

bool TrendExpert::Init(string symbol,ENUM_TIMEFRAMES period,bool every_tick,ulong magic)
{
  r_starttime = 0;
  r_endtime = 0;
  m_allow_hedge = false;
  m_exit_on_signal = false;
  m_allow_multiple_trades = false;
  return CExpert::Init(symbol, period, every_tick, magic);
}

bool TrendExpert::CheckOpen(void)
{
  bool ret = false;
  // If we are hedging, only allow single position in each direction?
  // TODO use m_extended_positioninfo to fix this!!!
  bool long_ok = false;
  bool short_ok = false;

  if (m_allow_multiple_trades) {
    long_ok = true;
    short_ok = true;
  } else {
    // Multiple trades not allowed
    // Long ok if there are no positions
    long_ok = !m_extended_positioninfo.SelectByDirectionAndMagic(m_symbol.Name(), m_magic, POSITION_TYPE_BUY);

    // short ok if there are no positions
    short_ok = !m_extended_positioninfo.SelectByDirectionAndMagic(m_symbol.Name(), m_magic, POSITION_TYPE_SELL);
  }

  if (long_ok) {
    if (CheckOpenLong()) {
      ret = true;
    }
  }

  if (short_ok) {
    if (CheckOpenShort()) {
      ret = true;
    }
  }

  // Removed from TrenderSignal/ExpertSignal::CheckOpenLong and CheckOpenShort on signal and moved here
  m_main_signal.resetPrices();
  // m_signal.BasePrice(0.0);
  //--- return without operations
  return ret;
}

void TrendExpert::OnTick(void)
{
  //--- check process flag
  if (!m_on_tick_process)
    return;

  // Need to check trailing here, as refresh limits to checking for registered
  // Time frames, which is logical.
  // TODO: Remove 'lastbar' checks on indicators?
  // if (SelectPosition())
  // {
  //   CheckTrailingStop();
  // }
  //--- updated quotes and indicators
  if (!Refresh())
    return;
  // Store time for start/end
  if (r_starttime == 0) {
    r_starttime = TimeCurrent();
  }
  r_endtime = TimeCurrent();

  //--- expert processing
  Processing();
}

bool TrendExpert::Refresh(void)
{
  MqlDateTime time;
  //--- refresh rates
  if(!m_symbol.RefreshRates())
    return false;

  //--- check need processing
  TimeToStruct(m_symbol.Time(),time);
  if(m_period_flags!=WRONG_VALUE && m_period_flags!=0)
    if((m_period_flags&TimeframesFlags(time))==0)
      return false;

  m_last_tick_time=time;

  //--- refresh indicators
  m_indicators.Refresh();

  // Hack checks
  // int idx = StartIndex();
  // MqlDateTime time2, time3;

  // datetime d1 = m_symbol.Time();

  // datetime d2 = Time(idx);
  // TimeToStruct(d2,time2);

  // datetime d3 = Time(0);
  // TimeToStruct(d3, time3);

  //--- ok
  return true;
}

//+------------------------------------------------------------------+
//| Main function                                                    |
//+------------------------------------------------------------------+
bool TrendExpert::Processing(void) {
  //--- calculate signal direction once
  m_signal.SetDirection();

  //--- check if open positions
  if (SelectPosition()) {
    //--- check the possibility of closing the position/delete pending orders
    if (m_exit_on_signal) {
      if (!CheckClose()) {
        CheckTrailingStop();
      }
    } else {
      CheckTrailingStop();
    }

    if (!m_allow_hedge) {
      return true;
    }
  }
/*
  // Tester does not place pending orders, as it buys market.
  // //--- check if placed pending orders
  // int total = OrdersTotal();
  // if (total != 0)
  // {
  //   for (int i = total - 1; i >= 0; i--)
  //   {
  //     m_order.SelectByIndex(i);
  //     if (m_order.Symbol() != m_symbol.Name())
  //       continue;
  //     //--- return without operations
  //     if (!m_allow_hedge) {
  //       return false;
  //     }
  //   }
  // }
*/
  //--- check the possibility of opening a position/setting pending order
  if (CheckOpen())
    return true;

  //--- return without operations
  return false;
}

